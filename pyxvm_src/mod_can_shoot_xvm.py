# from http://www.koreanrandom.com/forum/topic/29424-/?p=310192

print 'CanShoot http://www.koreanrandom.com/forum/topic/29424-/'



import BigWorld

from Math import Matrix, Vector3
from threading import Lock

from Avatar import PlayerAvatar

from xfw import registerEvent, getBattleApp



ctrlMode = ''
gunPosY = {}
ownGunPosY = 2.5
indicators = set()
indicatorUpdateStarted = False
lock = Lock()
enemiesList = set()
dynamicActive = set()



def setCanShoot(vID, flag):
    battle = getBattleApp()
    if battle is not None:
        markersManager = battle.markersManager
        vehiclesMarkers = markersManager._MarkersManager__plugins.getPlugin('vehicles')._VehicleMarkerPlugin__vehiclesMarkers
        if vID in vehiclesMarkers:
            marker = vehiclesMarkers[vID]
            markersManager.invokeMarker(marker.getMarkerID(), 'setSpeaking', [flag])



def isBattleOn():
    return getBattleApp() is not None



def isOnArena(vID):
    if not isBattleOn():
        return False
    return vID in enemiesList



def isLive(vID):
    if not isBattleOn():
        return False
    player = BigWorld.player()
    if vID not in player.arena.vehicles:
        return False
    return player.arena.vehicles[vID]['isAlive']



def isFriendly(vID):
    if not isBattleOn():
        return False
    player = BigWorld.player()
    playerID = player.playerVehicleID
    arenaVehicles = player.arena.vehicles
    return arenaVehicles[vID]['team'] == arenaVehicles[playerID]['team']



def clear():
    global enemiesList, dynamicActive
    enemiesList.clear()
    dynamicActive.clear()



def add_dir_indicator(vID):
    global indicators, lock, indicatorUpdateStarted
    indicators.add(vID)

    def refresh_indicator():
        global gunPosY, dynamicActive, ctrlMode, ownGunPosY
        deleteList = set()
        for vID in indicators:
            stop_ind = True
            if isOnArena(vID) and isLive(vID) and not isFriendly(vID):
                matrix = BigWorld.entities[vID].matrix
                if matrix:
                    m = Matrix(matrix)
                    pos = m.translation
                    if ctrlMode == 'strategic':
                        len = (BigWorld.camera().position - pos).length
                    else:
                        len = (BigWorld.player().position - pos).length
                    stop_ind = False
            if stop_ind:
                deleteList.add(vID)
                if vID in dynamicActive:
                    dynamicActive.remove(vID)
            else:
                player = BigWorld.player()
                Info = player.arena.vehicles.get(vID)
                enemyGunPosY = 2.5
                if vID in gunPosY:
                    enemyGunPosY = gunPosY[vID]
                collide = BigWorld.wg_collideSegment(
                    player.spaceID,
                    Vector3(0, ownGunPosY, 0) + player.position,
                    Vector3(0, enemyGunPosY, 0) + pos,
                    128
                )
                if collide:
                    if vID in dynamicActive:
                        setCanShoot(vID, False)
                        dynamicActive.remove(vID)
                elif vID not in dynamicActive:
                    setCanShoot(vID, True)
                    dynamicActive.add(vID)

        for vID in deleteList:
            indicators.remove(vID)

        BigWorld.callback(0.02, refresh_indicator)

    lock.acquire()
    try:
        if not indicatorUpdateStarted:
            indicatorUpdateStarted = True
            refresh_indicator()
    finally:
        lock.release()



def _InputHandler_onCameraChanged(eMode, curVehID = None):
    global ctrlMode
    ctrlMode = eMode



def _Arena_onVehicleKilled(targetID, attackerID, equipmentID, reason):
    global enemiesList
    if targetID in enemiesList:
        enemiesList.remove(targetID)



@registerEvent(PlayerAvatar, 'onEnterWorld')
def _PlayerAvatar_onEnterWorld(self, prereqs):
    self.arena.onVehicleKilled += _Arena_onVehicleKilled
    self.inputHandler.onCameraChanged += _InputHandler_onCameraChanged
    clear()



@registerEvent(PlayerAvatar, 'onLeaveWorld')
def _PlayerAvatar_onLeaveWorld(self):
    self.arena.onVehicleKilled -= _Arena_onVehicleKilled
    self.inputHandler.onCameraChanged -= _InputHandler_onCameraChanged
    clear()



@registerEvent(PlayerAvatar, 'vehicle_onEnterWorld')
def _PlayerAvatar_vehicle_onEnterWorld(self, vehicle):
    global gunPosY, ownGunPosY, enemiesList
    typeDesc = vehicle.typeDescriptor
    vID = vehicle.id
    hullPosY = typeDesc.chassis['hullPosition'][1]
    turretPosY = typeDesc.hull['turretPositions'][0][1]
    gunPosY[vID] = hullPosY + turretPosY + typeDesc.turret['gunPosition'][1]
    if vID == self.playerVehicleID:
        ownGunPosY = gunPosY[vID]
    if not isLive(vID):
        return
    if isFriendly(vID):
        return
    enemiesList.add(vID)
    add_dir_indicator(vID)



@registerEvent(PlayerAvatar, 'vehicle_onLeaveWorld')
def _PlayerAvatar_vehicle_onLeaveWorld(self, vehicle):
    global enemiesList, dynamicActive
    vID = vehicle.id
    if vID in enemiesList:
        enemiesList.remove(vID)
    if vID in dynamicActive:
        dynamicActive.remove(vID)
